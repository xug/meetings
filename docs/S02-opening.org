#+LaTeX_CLASS: beamer
#+BEAMER_THEME: Boadilla
#+PROPERTY: header-args:ditaa :eval no
#+OPTIONS: toc:nil 

#+TITLE: XUG / Season 02 opening
#+DATE: November 7th 2019

* XUG ?

- Mostly people from the D1\footnote{D1-Half-Day Nov. 29} departement for now

- Gather experimenters
  + (distributed) system experiments (e.g users of Grid'5000, other testbeds)
  + simulation + model validation (e.g users of Simgrid, Igrida)

- Discussions around practices towards better reproducible evaluations
  + We have a web page: https://xug.gitlabpages.inria.fr/meetings/
  + Announces are made by email
  
* Reproducible evaluations ?

ACM definition\footnote{https://www.acm.org/publications/policies/artifact-review-badging}:
- *Repeatability*: 
  Can someone in my team use my artifact using the exact same experimental setup and get similar results ?\\
  e.g., I (or my teammates) can repeat my own experiment on the same Grid'5000 machines.
- *Replicability*: 
  Can someone else from another team on another location use my articact and get similar results ?\\
  e.g., Can my friend using another testbed than Grid'5000 redo my experiment. 
- *Reproducibility*:\\
  Can someone else build her own artifact (e. g. from the information in the paper),
  use her own platform and get similar results ?

* Reproducible evaluations ?


ACM definition\footnote{https://www.acm.org/publications/policies/artifact-review-badging}:

- Space dimension (ACM is providing badges at a given point in time)
- What about the time dimension: Will it be possible to redo
  (repeat/replicate/reproduce) an experiment in 10 years ?

\\
\rightarrow this is though !

* What has been done last year 1/3

Typical session
- Someone presents something ...
  + could be a feedback on an experimental campaign
  + could be an on-going work (not finished experimental campaign)
  + could be a plan for an experimental campaign
- ... while the other attendees initiate
  + questions
  + exchanges about the tooling and good practices

* What has been done last year 2/3


- 6 sessions (6 months)
  + quick summary on https://xug.gitlabpages.inria.fr/meetings/

- Two tutorial sessions:
  + Grid'5000 for beginner (Getting Started)
  + Nix tutorial: management of replicable software environment

* What has been done last year 3/3

- We use this picture as a common basis for 
  our discussion\footnote{\tiny https://github.com/alegrand/SMPE/blob/master/lectures/1\_reproducible\_research.pdf}

[[./workflow.png]]

* What do we plan for this year ?

- Presentations
  + volonteers ?

- Tutorials
  + specific needs ?

- State of the art discussions
  + paper selection ?

- Other Ideas ?
  + ...


* Misc information


There's a new forum https://reproducible-research.inria.fr/

